import React from 'react';
import loadable from '@loadable/component';

const App = loadable(() => import('../client/app'));
const Home = loadable(() => import('../client/screens/home'));
const Detail = loadable(() => import('../client/screens/detail'));
const NotFound = loadable(() => import('../client/screens/notfound'));
const Owned = loadable(() => import('../client/screens/owned'));

export default [
  {
    component: App,
    fetchData: Home.fetchData,
    routes: [
      {
        component: Home,
        path: '/',
        exact: true
      },
      {
        component: Detail,
        path: '/detail/:name',
        exact: true
      },
      {
        component: Owned,
        path: '/my-pokemon',
        exact: true
      },
      {
        path: '*',
        component: NotFound
      }
    ]
  }
];

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import configRoutes from '../common/configRoutes';
import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache
} from '@apollo/client';
import { StorageProvider } from '../client/contexts';
import { loadableReady } from '@loadable/component';
import fetch from 'cross-fetch';

const client = new ApolloClient({
  ssrForceFetchDelay: 100,
  ssrMode: true,
  link: createHttpLink({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
    fetch: fetch
  }),
  cache: new InMemoryCache().restore(window.__APOLLO_STATE__ || {})
});

loadableReady(() => {
  ReactDOM.hydrate(
    <ApolloProvider client={client}>
      <StorageProvider>
        <BrowserRouter>{renderRoutes(configRoutes)}</BrowserRouter>
      </StorageProvider>
    </ApolloProvider>,
    document.querySelector('#root')
  );
});

import React, {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useState
} from 'react';
import storageReducer, { initialState, ACTIONS } from './reducer';

const StorageStateContext = createContext({});
const StorageActionContext = createContext({});

const storageName = 'pokemon';

const StorageProvider = ({ children }) => {
  const [state, dispatch] = useReducer(storageReducer, initialState);
  const [reload, setReload] = useState(false);

  const get = () => {
    const data = JSON.parse(window.localStorage.getItem(storageName)) || [];
    dispatch({ type: ACTIONS.SET_INITIAL, payload: data });
  };

  const save = (data) => {
    const result = [...state.data, data];
    window.localStorage.setItem(storageName, JSON.stringify(result));
    dispatch({ type: ACTIONS.SET_DATA, payload: data });
  };

  const release = (name) => {
    const result = state.data.filter((st) => {
      return st.pokemonName !== name;
    });

    window.localStorage.setItem(storageName, JSON.stringify(result));
    dispatch({ type: ACTIONS.SET_RELEASE, payload: name });
    setReload(true);
  };

  useEffect(() => {
    get();
    if (reload) {
      setReload(false);
    }
  }, [reload]);

  const actions = {
    save,
    release
  };

  return (
    <StorageStateContext.Provider value={{ state }}>
      <StorageActionContext.Provider value={actions}>
        {children}
      </StorageActionContext.Provider>
    </StorageStateContext.Provider>
  );
};

const useStorageState = () => {
  const context = useContext(StorageStateContext);
  if (context === undefined) {
    throw new Error('useStorageState must be used within a StorageProvider');
  }
  return context;
};

const useStorageAction = () => {
  const context = useContext(StorageActionContext);
  if (context === undefined) {
    throw new Error('useStorageAction must be used within a StorageProvider');
  }
  return context;
};

export { StorageProvider, useStorageState, useStorageAction };

const STATUS = {
  IDLE: 'IDLE',
  RESOLVED: 'RESOLVED',
  REJECTED: 'REJECTED'
};

export const ACTIONS = {
  SET_INITIAL: 'SET_INITIAL',
  SET_DATA: 'SET_DATA',
  SET_RELEASE: 'SET_RELEASE',
  SET_ERROR: 'SET_ERROR'
};

export const initialState = {
  status: STATUS.IDLE,
  error: '',
  data: []
};

export default function commentReducer(state, action) {
  switch (action.type) {
    case ACTIONS.SET_INITIAL:
      return { ...initialState, status: STATUS.RESOLVED, data: action.payload };
    case ACTIONS.SET_DATA:
      return {
        ...initialState,
        status: STATUS.RESOLVED,
        data: [...state.data, action.payload]
      };
    case ACTIONS.SET_RELEASE:
      const data = state.data.filter((st) => {
        return st.pokemonName !== name;
      });
      return {
        status: STATUS.RESOLVED,
        data
      };
    default:
      return state;
  }
}

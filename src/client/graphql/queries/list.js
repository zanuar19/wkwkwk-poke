import { gql } from '@apollo/client';

export const list_query = gql`
  query pokemons($limit: Int, $offset: Int) {
    pokemons(limit: $limit, offset: $offset) {
      count
      nextOffset
      results {
        id
        name
        image
      }
    }
  }
`;

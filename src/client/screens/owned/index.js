import React, { useState, useEffect } from 'react';
import { useStorageState, useStorageAction } from '../../contexts';
import Card from '../../components/card';
import { getImage } from '../../utils';
import Meta from '../../components/meta';

const Owned = () => {
  const { state } = useStorageState();
  const { release } = useStorageAction();

  const [data, setData] = useState([]);

  const onRelease = (name) => {
    release(name);
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setData(state?.data);
    }
  }, [state]);

  if (!data.length) {
    return (
      <div className="container-center">
        <p>You don't have pokemon</p>
      </div>
    );
  }

  return (
    <>
      <Meta
        title={`owned - pokemon pokemon dimana kamu`}
        overview="pokemon pokemon dimana kamu"
      />
      <div className="container-list">
        {data.map((item, index) => {
          const badges = item.pokemonDetail.types
            ? item.pokemonDetail.types.map((type) => {
                return type.type.name;
              })
            : [];
          return (
            <Card
              key={index}
              title={item.pokemonName}
              to={`/detail/${item.pokemonDetail.name}`}
              badges={badges}
              image={getImage(item.pokemonDetail.id)}
              isWithRelease={true}
              onRelease={() => onRelease(item.pokemonName)}
            />
          );
        })}
      </div>
    </>
  );
};

export default Owned;

import React from 'react';
import Meta from '../../components/meta';

const NotFound = () => {
  return (
    <>
      <Meta
        title={`Not found - pokemon pokemon dimana kamu`}
        overview="pokemon pokemon dimana kamu"
      />
      <div className="container-center">
        <p>404</p>
      </div>
    </>
  );
};

export default NotFound;

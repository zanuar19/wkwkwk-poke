import React from 'react';
import PageNotFound from '../index';
import { render as rtlRender, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Simulate render 404 not found', () => {
  test('render default', () => {
    const { container } = rtlRender(<PageNotFound />);
    expect(container).toMatchSnapshot();
  });
  test('get text 404', () => {
    rtlRender(<PageNotFound />);
    expect(screen.getByText(`404`)).toBeInTheDocument();
  });
});

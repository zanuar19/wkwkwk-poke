import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { detail_query } from '../../graphql/queries/detail';
import PokemonMove from '../../components/pokemon-move';
import { getImage } from '../../utils';
import { useStorageAction, useStorageState } from '../../contexts';
import Input from '../../components/input';
import Button from '../../components/button';
import Badge from '../../components/badge';
import Modal from '../../components/modal';
import Meta from '../../components/meta';

const Detail = () => {
  const { name } = useParams();
  const { state } = useStorageState();
  const { save } = useStorageAction();
  const [visibleModalSuccess, setVisibleModalSuccess] = useState(false);
  const [visibleModalFailed, setVisibleModalFailed] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [isWaitCatch, setIsWaitCatch] = useState(false);
  const [pokemonName, setPokemonName] = useState('');
  const [message, setMessage] = useState('');

  const { loading, data } = useQuery(detail_query, {
    variables: {
      name
    }
  });

  if (loading) {
    return (
      <div className="container-loader">
        <div className="loader"></div>
      </div>
    );
  }

  const onThrow = () => {
    setIsWaitCatch(true);
    setTimeout(() => {
      setIsWaitCatch(false);

      if (Math.random() > 0.5) {
        setVisibleModalSuccess(true);
      } else {
        setVisibleModalFailed(true);
      }
    }, 3000);
  };

  const onCloseModalSuccess = () => {
    setVisibleModalSuccess(false);
  };

  const onCloseModalFailed = () => {
    setVisibleModalFailed(false);
  };

  const onChangeName = (e) => {
    setPokemonName(e.target.value);
    const result = state.data.filter((st) => {
      return st.pokemonName === e.target.value;
    });
    if (result.length > 0) {
      setDisabled(true);
      setMessage('Name already used');
    } else {
      setDisabled(false);
    }
  };

  const onSubmit = () => {
    if (!pokemonName) {
      setDisabled(true);
      setMessage('Name is required');
    } else {
      const result = {
        pokemonName,
        pokemonDetail: data.pokemon
      };

      save(result);
      setDisabled(false);
      setPokemonName('');
      setVisibleModalSuccess(false);
    }
  };

  return (
    <>
      <Meta
        title={`${data.pokemon.name} - pokemon pokemon dimana kamu`}
        overview="pokemon pokemon dimana kamu"
      />
      <div className="detail-card">
        <div className="detail-card-title">{data.pokemon.name}</div>
        <div className="detail-card-media">
          <img src={getImage(data.pokemon.id)} alt="" />
        </div>
        <div>
          {data?.pokemon ? (
            data.pokemon.types.map((type, index) => (
              <Badge key={index} name={type.type.name} />
            ))
          ) : (
            <></>
          )}
        </div>
      </div>
      <div className="container-move">
        <Button
          text="Catch"
          onClick={() => {
            onThrow();
          }}
        />
      </div>
      {data?.pokemon && <PokemonMove moves={data.pokemon.moves} />}
      {isWaitCatch && (
        <div className="container-loader">
          <div className="loader"></div>
        </div>
      )}

      <Modal
        visible={visibleModalSuccess}
        onClose={() => onCloseModalSuccess()}
      >
        <p>Congratulation..</p>
        {disabled && (
          <div className="warning">
            <p>{message}</p>
          </div>
        )}
        <Input
          value={pokemonName}
          placeholder="type name"
          onChange={(e) => onChangeName(e)}
        />
        <Button
          text="Submit"
          disabled={disabled}
          onClick={() => {
            onSubmit();
          }}
        />
      </Modal>
      <Modal visible={visibleModalFailed} onClose={() => onCloseModalFailed()}>
        <p>Try again..</p>
        <p>You will get if you focus..</p>
      </Modal>
    </>
  );
};

export default Detail;

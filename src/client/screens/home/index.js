import React, { memo } from 'react';
import loadable from '@loadable/component';
import { useStorageState } from '../../contexts';
import Button from '../../components/button';
import Meta from '../../components/meta';
import usePokemons from '../../hooks/usePokemons';

const Card = loadable(() => import('../../components/card'));

const Home = () => {
  const { state } = useStorageState();
  const {
    pokemons,
    loading,
    loadMore,
    isShowLoadMore,
    isDisabledLoadMore
  } = usePokemons();

  const owned = (id) => {
    const data = state?.data
      ? state.data.filter((st) => {
          return st.pokemonDetail.id === id;
        })
      : [];
    return data.length;
  };

  if (loading) {
    return (
      <div className="container-loader">
        <div className="loader"></div>
      </div>
    );
  }

  return (
    <>
      <Meta
        title="pokemon pokemon dimana kamu"
        overview="pokemon pokemon dimana kamu"
      />
      <div className="container-list">
        {pokemons &&
          pokemons.map((item, index) => {
            const badges = [`${owned(item.id)} owned`];
            return (
              <Card
                key={index}
                title={item.name}
                to={`/detail/${item.name}`}
                badges={badges}
                image={item.image}
              />
            );
          })}
      </div>

      {!loading && isShowLoadMore && (
        <div className="container-main">
          <Button
            text={`${isDisabledLoadMore ? 'Loading....' : 'Load more'}`}
            disabled={isDisabledLoadMore}
            onClick={() => loadMore()}
          />
        </div>
      )}
    </>
  );
};

export default memo(Home);

import React from 'react';
import Home from '../index';
import { render as rtlRender } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { BrowserRouter } from 'react-router-dom';
import fetch from 'cross-fetch';

import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache
} from '@apollo/client';

const client = new ApolloClient({
  ssrForceFetchDelay: 100,
  ssrMode: true,
  link: createHttpLink({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
    fetch: fetch
  }),
  cache: new InMemoryCache().restore(window.__APOLLO_STATE__ || {})
});

describe('Simulate render home', () => {
  test('render default', () => {
    const { container } = rtlRender(
      <ApolloProvider client={client}>
        <BrowserRouter>
          <Home />
        </BrowserRouter>
      </ApolloProvider>
    );
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
import { Helmet } from 'react-helmet';

const Meta = ({ title, overview }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={overview} itemprop="description" />
      <meta
        name="keywords"
        content
        itemprop={`tokopedia, legal film, react, redux, express, nodejs, server side rendering, ${title}`}
      />
    </Helmet>
  );
};

export default Meta;

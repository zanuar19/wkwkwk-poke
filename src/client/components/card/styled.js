import styled from '@emotion/styled';

export const Container = styled.div`
  font-weight: 300;
  box-shadow: -8px 8px 14px 0 rgb(25 42 70 / 11%);
  border: 2px solid #ececec;
  margin: 10px;
  padding: 20px;
  border-radius: 8px;
  min-width: 230px;
`;

export const CompCard = styled.div`
  display: flex;
  align-items: center;
`;

export const LeftSide = styled.div`
  flex: 1;
`;

export const Title = styled.div`
  font-weight: 300;
  margin-bottom: 20px;
`;

export const Media = styled.div`
  width: 96px;
  height: 96px;
`;

export const ButtonInner = styled.div`
  padding: 20px;
`;

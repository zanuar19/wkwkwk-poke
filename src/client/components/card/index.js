import React, { memo } from 'react';
import { Container, CompCard, LeftSide, Media, Title } from './styled';
import { Link } from 'react-router-dom';
import { css } from '@emotion/react';
import loadable from '@loadable/component';

const Badge = loadable(() => import('../badge'));
const Button = loadable(() => import('../button'));

const Card = ({ to, title, badges, image, isWithRelease, onRelease }) => {
  return (
    <div className="column">
      <Container>
        <Link to={to}>
          <CompCard>
            <LeftSide>
              <Title>{title}</Title>
              <div>
                {badges ? (
                  badges.map((badge, index) => {
                    return <Badge key={index} name={badge} />;
                  })
                ) : (
                  <></>
                )}
              </div>
            </LeftSide>
            <Media>
              <img
                css={css`
                  width: 95px;
                  height: 95px;
                `}
                src={image}
                alt={title}
              />
            </Media>
          </CompCard>
        </Link>
        {isWithRelease && (
          <div>
            <Button text="Release" onClick={onRelease} />
          </div>
        )}
      </Container>
    </div>
  );
};

export default memo(Card);

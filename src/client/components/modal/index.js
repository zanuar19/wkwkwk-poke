import React, { useState } from 'react';
import { Button, Span } from './styled';

const Modal = ({ children, visible, onClose }) => {
  const [isVisible, setIsVisible] = useState(visible);

  const onCloseModal = () => {
    setIsVisible(false);
    onClose();
  };

  const classShow = isVisible || visible ? 'show-modal' : '';

  return (
    <div className={`modal ${classShow} `}>
      <div className="modal-content">
        <span className="close" onClick={() => onCloseModal()}>
          &times;
        </span>
        {children}
      </div>
    </div>
  );
};

export default Modal;

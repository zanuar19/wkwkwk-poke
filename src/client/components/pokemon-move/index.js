import React from 'react';
import { Container } from './styled';
import Badge from '../badge';

const Move = ({ moves }) => {
  return (
    <Container>
      {moves.map((move, index) => {
        return <Badge key={index} name={move.move.name} />;
      })}
    </Container>
  );
};

export default Move;

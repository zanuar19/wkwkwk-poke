import styled from '@emotion/styled';

export const Container = styled.div`
  display: flex;
  margin: 20px;
  border: 2px solid #ececec;
  border-radius: 8px;
  padding: 20px;
  box-shadow: -8px 8px 14px 0 rgb(25 42 70 / 11%);
  flex-wrap: wrap;
`;

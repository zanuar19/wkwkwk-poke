import React from 'react';
import { Container } from './styled';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <header>
      <Container>
        <Link className="white flex1" to="/">
          Home
        </Link>
        <Link className="white" to="/my-pokemon">
          My Pokemon
        </Link>
      </Container>
    </header>
  );
};

export default Header;

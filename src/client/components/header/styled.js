import styled from '@emotion/styled';

export const Container = styled.div`
  background: #1265ad;
  height: 50px;
  display: flex;
  padding: 10px 20px;
`;

import React from 'react';
import Badge from '../index';
import { render as rtlRender } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Simulate badge component', () => {
  test('render with name mantap', () => {
    const { container } = rtlRender(<Badge name="mantap" />);
    expect(container).toMatchSnapshot();
  });
});

import styled from '@emotion/styled';

export const Container = styled.div`
  font-weight: 600;
  font-size: 12px;
  display: inline-block;
  background: #960000;
  color: #fff;
  padding: 5px 10px;
  margin: 0 10px 10px 0;
  border-radius: 4px;
`;

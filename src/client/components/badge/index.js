import React from 'react';
import { Container } from './styled'

const Badge = ({name}) => {
  return (
    <Container>
      {name}
    </Container>
  );
};

export default Badge;

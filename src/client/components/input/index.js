import React from 'react';
import { Container, Input } from './styled';

const InputComp = (props) => {
  return (
    <Container>
      <Input {...props} />
    </Container>
  );
};

export default InputComp;

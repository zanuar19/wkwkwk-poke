import styled from '@emotion/styled';

export const Input = styled.input`
  flex: 1;
  border-radius: 8px;
  border: 1px solid #e9e9f5;
  padding-left: 10px;
  height: 50px;
  :focus {
    outline: none;
    border: 1px solid #373767;
    color: #333333;
    font-family: 'Axiforma';
    font-size: 12px;
  }
  &:hover {
    border: 1px solid #5f5f5f;
  }
  ::placeholder {
    font-family: 'Axiforma';
    font-size: 12px;
    color: #a1cbe1;
  }
`;

export const Container = styled.div`
  margin: 20px 0;
  display: flex;
  align-content: stretch;
`;

import React from 'react';
import { Button, Span } from './styled';

const Buttons = (props) => {
  const { text, disabled, onClick } = props;

  return (
    <Button disabled={disabled} onClick={onClick}>
      {text}
    </Button>
  );
};

export default Buttons;

import React from 'react';
import Button from '../index';
import { render as rtlRender } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Simulate button component', () => {
  test('render with text', () => {
    const { container } = rtlRender(<Button text="mantap" disabled={false} />);
    expect(container).toMatchSnapshot();
  });
});

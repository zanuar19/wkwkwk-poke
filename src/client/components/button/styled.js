import styled from '@emotion/styled';

export const Button = styled.button`
  border-radius: 8px;
  border: 1px solid #e9e9f5;
  width: 100%;
  padding: 8px;
  margin: 10px 0;
  font-weight: 600;
  font-family: inherit;
  background: #1265ad;
  color: #fff;
  &:hover {
    border: 1px solid #5f5f5f;
    background: #fb6d6c;
    color: #fff;
  }
`;

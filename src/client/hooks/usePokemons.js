import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { list_query } from '../graphql/queries/list';

const limit = 12;

const usePokemons = () => {
  const [pokemons, setPokemons] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isShowLoadMore, setIsShowLoadMore] = useState(false);
  const [isDisabledLoadMore, setIsDisabledLoadMore] = useState(false);

  const { loading, data, fetchMore } = useQuery(list_query, {
    variables: {
      limit,
      offset: 0
    },
    onCompleted: () => {
      setIsShowLoadMore(true);
      setPokemons(data.pokemons.results);
      setOffset(data.pokemons.nextOffset);
    }
  });

  const loadMore = async () => {
    if (!isDisabledLoadMore) {
      setIsDisabledLoadMore(true);
      const fetchedMore = await fetchMore({
        variables: { limit, offset }
      });
      setIsDisabledLoadMore(false);
      setOffset(fetchedMore.data.pokemons.nextOffset);
      setPokemons([...pokemons, ...fetchedMore.data.pokemons.results]);
    }
  };

  return {
    pokemons,
    setPokemons,
    offset,
    setOffset,
    loading,
    loadMore,
    isShowLoadMore,
    isDisabledLoadMore
  };
};

export default usePokemons;

import React from 'react';
import { renderRoutes } from 'react-router-config';
import Header from './components/header';
import './styles.scss';

const App = ({ route }) => {
  return (
    <>
      <Header />
      {renderRoutes(route.routes)}
    </>
  );
};

export default App;

import express from 'express';
import configRoutes from '../common/configRoutes';
import { renderRoutes } from 'react-router-config';
import fetch from 'cross-fetch';
import { renderToStringWithData } from '@apollo/client/react/ssr';
import {
  ApolloProvider,
  ApolloClient,
  createHttpLink,
  InMemoryCache
} from '@apollo/client';
import { StaticRouter } from 'react-router-dom';
import React from 'react';
import { ChunkExtractor, ChunkExtractorManager } from '@loadable/server';
import { Helmet } from 'react-helmet';
import path from 'path';

const app = express();
const port = process.env.PORT || 5400;
const webStats = path.resolve(__dirname, '../dist-client/loadable-stats.json');

app.set('view engine', 'ejs');
app.use(express.static('dist-client'));
app.use(express.static('public'));

app.get('*', (req, res) => {
  const client = new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
      uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
      credentials: 'same-origin',
      headers: {
        cookie: req.header('Cookie')
      },
      fetch: fetch
    }),
    cache: new InMemoryCache()
  });

  const context = {};

  const webExtractor = new ChunkExtractor({ statsFile: webStats });

  const App = (
    <ChunkExtractorManager extractor={webExtractor}>
      <ApolloProvider client={client}>
        <StaticRouter location={req.url} context={context}>
          {renderRoutes(configRoutes)}
        </StaticRouter>
      </ApolloProvider>
    </ChunkExtractorManager>
  );

  renderToStringWithData(App).then((content) => {
    const initialState = client.extract();
    const scriptTags = webExtractor.getScriptTags();
    const linkTags = webExtractor.getLinkTags();
    const styleTags = webExtractor.getStyleTags();
    const helmet = Helmet.renderStatic();
    res.render('index', {
      content,
      helmet,
      initialState,
      scriptTags,
      linkTags,
      styleTags
    });
  });
});
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

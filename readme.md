# wkwkwk-poke

## live

[wkwkwk poke](https://wkwkwk-poke.herokuapp.com/)

## requirement

- node 14.15
- npm 6.14.8

## install

- npm install

## how to run

### development

```sh
npm run dev
```

### production

```sh
npm run prod
```

## features

- Mobile First
- Webpack
- React with Hooks and Context
- GraphQL
- Emotion
- Jest and React Testing Library
- PWA, SPA & SSR

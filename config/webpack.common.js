module.exports = {
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-react',
                { runtime: 'automatic', importSource: '@emotion/react' }
              ],
              '@babel/preset-env'
            ],
            plugins: [
              '@emotion/babel-plugin',
              '@babel/plugin-transform-runtime',
              '@loadable/babel-plugin'
            ]
          }
        }
      }
    ]
  }
};

const { merge } = require('webpack-merge');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { GenerateSW } = require('workbox-webpack-plugin');
const LoadablePlugin = require('@loadable/webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');

const baseConfig = require('./webpack.common');

const devConfig = {
  module: {
    rules: [
      {
        test: /\.(s(a|c)ss)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(woff|ttf|eot|svg|png)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
        use: {
          loader: 'url-loader'
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg|png)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  },
  entry: './src/client/index.js',
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, '../dist-client'),
    filename: '[name].client_bundle.js'
  },
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(),
    new LoadablePlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css'
    }),
    new AssetsPlugin({ removeFullPathAutoPrefix: true }),
    new GenerateSW({
      swDest: './service-worker.js'
    })
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /node_modules/,
          chunks: 'initial',
          filename: 'vendors.[contenthash].js',
          priority: 1,
          maxInitialRequests: 2,
          minChunks: 3
        }
      }
    }
  }
};

module.exports = merge(baseConfig, devConfig);
